﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LDLib;
using System.IO;

namespace Liars_Dice_Console
{
    class Program
    {
        static int GameCount = 0;
        const int GAMES_TO_PLAY = 100;
        const string FILE_PATH = @"D:\Depot\LiarsDiceResults.txt";
        static void Main(string[] args)
        {
            PlayGame().Wait();
        }

        private static async Task PlayGame()
        {
            Console.WriteLine("Welcome to Liar's Dice, a game of cunning and deceit");
            Console.WriteLine("How many humans will be playing?");
            var humanCount = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < humanCount; ++i)
            {
                GameState.Instance.AddPlayer(new HumanPlayer(5));
            }

            Console.WriteLine("You will play against 4 computers");
            GameState.Instance.AddPlayer(new Monte(5));
            GameState.Instance.AddPlayer(new RandomMonte(5));
            GameState.Instance.AddPlayer(new MaybeBluff(5));
            GameState.Instance.AddPlayer(new BluffMaster(5));


            while (GameCount < GAMES_TO_PLAY)
            {
                GameState.Instance.PlayAgain();
                while (!GameState.Instance.GameOver)
                {
                    GameState.Instance.RoundOver = false;
                    GameState.Instance.RollOut();
                    while (!GameState.Instance.RoundOver)
                    {
                        var move = await GameState.Instance.Play();
                        if (move.Human)
                        {
                            move = ObtainHumanMove();
                        }
                        Console.WriteLine(GameState.Instance.CurrentTurn.ToString() + move.ToString());
                        if (move.Call)
                        {
                            var result = GameState.Instance.EndRound();
                            Console.WriteLine(result.ToString());
                        }
                        else
                        {
                            GameState.Instance.EndTurn();
                        }
                    }
                }
                GameCount++;
            }

            SaveResults();
        }

        private static void SaveResults()
        {
            string results = "";
            foreach (var player in GameState.Instance.Players)
            {
                results += "\n" + player.ToString() + " Wins: " + player.Wins + " Win Percent: " + (float)player.Wins / GAMES_TO_PLAY + " ";
            }
            File.WriteAllText(FILE_PATH, results);
        }

        private static PlayerMove ObtainHumanMove()
        {
            Console.WriteLine("It's your turn, do you want to call? Y/N");
            var call = Console.ReadLine();
            if (call.ToUpper().Equals("Y"))
            {
                return new PlayerMove() { Call = true };
            }
            Console.WriteLine("What number do you guess?");
            var diceNumGuess = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("How many do you guess?");
            var totalNumGuess = Convert.ToInt32(Console.ReadLine());

            var move = new PlayerMove() { DiceNumber = diceNumGuess, GuessAmount = totalNumGuess };
            if(GameState.Instance.ValidMove(move))
            {
                GameState.Instance.AddMove(move);
                return move;
            }

            Console.WriteLine("That is not a valid move. Try again.");
            return ObtainHumanMove();
        }
    }
}
